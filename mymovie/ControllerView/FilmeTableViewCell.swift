//
//  FilmeTableViewCell.swift
//  mymovie
//
//  Created by Paulo Silva on 27/03/19.
//  Copyright © 2019 ph.mobi.br. All rights reserved.
//

import UIKit

class FilmeTableViewCell: UITableViewCell {

    @IBOutlet weak var imagem: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var sinopseLabel: UILabel!
    @IBOutlet weak var progress: KDCircularProgress!
    @IBOutlet weak var pontuacaoLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        customProgresson()

        // Configure the view for the selected state
    }
    
    func customProgresson(){
        progress.startAngle = -90
        progress.progressThickness = 0.5
        progress.trackThickness = 0.6
        progress.clockwise = true
        progress.gradientRotateSpeed = 70
        progress.roundedCorners = false
        progress.glowMode = .forward
        progress.glowAmount = 0.9
    }

}
