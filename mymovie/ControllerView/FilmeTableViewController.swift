//
//  FilmeTableViewController.swift
//  mymovie
//
//  Created by Paulo Silva on 27/03/19.
//  Copyright © 2019 ph.mobi.br. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage



class FilmeTableViewController: UITableViewController {
    
    var progress: KDCircularProgress!
    var filme = Filme()
    var listaFilmes : [Results] = []
    var controller = FilmeController()
    var isRefreshing : Bool = false
    var numberPage : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getFilmes(number: numberPage)
    }
    
    func getFilmes(number: Int) {
        self.isRefreshing = true
        controller.fetchEvento(number: numberPage){ (jsonResult) in
            if jsonResult != nil{
             self.filme = Filme(json:jsonResult!)
             self.numberPage += 1
             self.isRefreshing = false
             if self.filme.results! != nil || !self.filme.results!.isEmpty{
                self.listaFilmes += self.filme.results!
              }
               self.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.listaFilmes.count > 0{
            return self.listaFilmes.count
        }else{
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "identifierFilmeCell", for: indexPath) as! FilmeTableViewCell
        
        let item = listaFilmes[indexPath.row]
        cell.tituloLabel.text = item.title!
        cell.sinopseLabel.text = item.overview
        
        let pontuacao = String(describing: item.voteAverage!)
        let pontos = pontuacao.split(separator: ".")
        let pont = Double( 36 *  NSInteger(String(pontos[0]+pontos[1]))! / 10)
        if pont > 252{
            cell.progress.set(colors:  UIColor(red: 33/255, green: 208/255, blue: 122/255, alpha: 1.0))
        }else{
            cell.progress.set(colors: UIColor(red: 210/255, green: 213/255, blue: 49/255, alpha: 1.0))
        }
        cell.progress.angle = pont

        if String(pontos[0]+pontos[1]) == "00"{
            cell.pontuacaoLabel.text = "NR"
        }else{
            cell.pontuacaoLabel.text = String(pontos[0]+pontos[1])+"%"
        }
        if let poster_path = item.posterPath {
            cell.imagem.sd_setImage(with: URL(string: (Memoria.URL_FOTO+poster_path)), completed: { (trioImage, error, cacheType, url) in
//                cell.activityIndicatorLoadingImage.stopAnimating()
            })
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.listaFilmes[indexPath.row]
        self.performSegue(withIdentifier: "detalheFilmeSegue", sender: item)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detalheFilmeSegue" {
            let detalheFilmeView: DetalheFilmeTableViewController = segue.destination as! DetalheFilmeTableViewController
            let indexPath = self.tableView.indexPathForSelectedRow!
            if !listaFilmes.isEmpty{
                detalheFilmeView.filmeSelecionado = listaFilmes[indexPath.row]
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height{
            if !isRefreshing {
                getFilmes(number: numberPage+1)
            }
        }
    }
    
    private struct SerializationKeysModel {
        static let result = "results"
    }
    
}
