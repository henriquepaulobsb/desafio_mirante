//
//  DetalheFilmeTableViewController.swift
//  mymovie
//
//  Created by Paulo Silva on 28/03/19.
//  Copyright © 2019 ph.mobi.br. All rights reserved.
//

import UIKit

class DetalheFilmeTableViewController: UITableViewController {
    
    @IBOutlet weak var imagemFilme: UIImageView!
    @IBOutlet weak var sinopseLabel: UILabel!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var pontuacaoLabel: UILabel!
    @IBOutlet weak var progressPont: KDCircularProgress!
    let imageView = UIImageView()
    
    var barButtonBack : UIBarButtonItem!
    var buttonBack : UIButton!
    
    var filmeSelecionado = Results()

    override func viewDidLoad() {
        super.viewDidLoad()
        initNavigationBar()
        
        if let poster_path = filmeSelecionado.posterPath {
            imagemFilme.sd_setImage(with: URL(string: (Memoria.URL_FOTO+poster_path)), completed: { (trioImage, error, cacheType, url) in
            })
        }
        
        sinopseLabel.text = filmeSelecionado.overview!
        let pontuacao = String(describing: filmeSelecionado.voteAverage!)
        let pontos = pontuacao.split(separator: ".")
        
        
        let pont = Double( 36 *  NSInteger(String(pontos[0]+pontos[1]))! / 10)
        if pont > 252{
            progressPont.set(colors:  UIColor(red: 33/255, green: 208/255, blue: 122/255, alpha: 1.0))
        }else{
            progressPont.set(colors: UIColor(red: 210/255, green: 213/255, blue: 49/255, alpha: 1.0))
        }
        progressPont.angle = pont
        
        if String(pontos[0]+pontos[1]) == "00"{
            pontuacaoLabel.text = "NR"
        }else{
            pontuacaoLabel.text = String(pontos[0]+pontos[1])+"%"
        }
        customProgresson()
        
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func customProgresson(){
        progressPont.startAngle = -90
        progressPont.progressThickness = 0.5
        progressPont.trackThickness = 0.6
        progressPont.clockwise = true
        progressPont.gradientRotateSpeed = 70
        progressPont.roundedCorners = false
        progressPont.glowMode = .forward
        progressPont.glowAmount = 0.9
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let imagemCrop =  imagemFilme.image?.getCropRatio()
        return tableView.frame.width * imagemCrop!

    }
    
    func initNavigationBar() {
        title = filmeSelecionado.title!
        buttonBack = UIButton(type: .system)
        buttonBack.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        buttonBack.setImage(UIImage(named: "Back"), for: UIControl.State())
        buttonBack.addTarget(self, action: #selector(self.barButtonBackAction), for: .touchDown)
//        buttonBack.sizeToFit()
         buttonBack.clipsToBounds = true
        buttonBack.tintColor = UIColor(red: 33/255, green: 208/255, blue: 122/255, alpha: 1.0)
        barButtonBack = UIBarButtonItem(customView: buttonBack)
        navigationItem.leftBarButtonItem = barButtonBack
        
    }
    
    @objc func barButtonBackAction () {
        if let navigation = self.navigationController {
            navigation.popViewController(animated: true)
        }
    }
    
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let y = 300 - (scrollView.contentOffset.y + 300)
//        let height = min(max(y, 60), 400)
//        imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
//    }
}

extension UIImage{
    func getCropRatio()-> CGFloat{
        let widthRatio = CGFloat(self.size.width / self.size.height)
        return widthRatio
    }
}
