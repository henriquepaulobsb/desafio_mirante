//
//  FilmeController.swift
//  mymovie
//
//  Created by Paulo Silva on 27/03/19.
//  Copyright © 2019 ph.mobi.br. All rights reserved.
//

import UIKit
import SwiftyJSON

class FilmeController: NSObject {
    
    public typealias Parameters = [String: Any]
    
    var service = Service()
    
    public func fetchEvento(number: Int, handlerJsonResult: @escaping (JSON?) -> ()) {
        service.getFilmesService(number: number, handlerJsonResult: { (jsonResult) -> () in
            handlerJsonResult(jsonResult)
        })
   }

}
