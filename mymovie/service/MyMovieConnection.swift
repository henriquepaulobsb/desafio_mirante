//
//  MyMovieConnection.swift
//  mymovie
//
//  Created by Paulo Silva on 27/03/19.
//  Copyright © 2019 ph.mobi.br. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


typealias ServiceResponse = (_ jsonResult: JSON?, _ error: NSError?) -> Void

typealias handlerResponseJSON = (Alamofire.DataResponse<Any>) -> Swift.Void
typealias handlerDownloadResponseData = (Alamofire.DownloadResponse<Data>) -> Swift.Void

class MyMovieConnection {
    static let sharedConnection = MyMovieConnection()
    
    var headers : HTTPHeaders?
    
    var dataHeaders : HTTPHeaders?
    
    var cookies = [HTTPCookie]()
    
    var stringCookies = ""
    
    var manager: SessionManager!
    
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 10
        manager = Alamofire.SessionManager(configuration: configuration)
    }
    
    static func request(_ url : String, responseJSON: @escaping handlerResponseJSON) {
        let data = Alamofire.request(url)
        data.responseJSON { (response) in
            responseJSON(response)
        }
    }
    
    static func getCookies(response : DataResponse<Any>) {
        if MyMovieConnection.sharedConnection.cookies.count == 0 {
            if let headerFields = response.response?.allHeaderFields as? [String: String], let url = response.request?.url {
                MyMovieConnection.sharedConnection.cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
                
                MyMovieConnection.sharedConnection.stringCookies = ""
                
                for cookie in MyMovieConnection.sharedConnection.cookies {
                    MyMovieConnection.sharedConnection.stringCookies += "\(cookie.name)=\(cookie.value);"
                    print("Cookie: \(MyMovieConnection.sharedConnection.stringCookies)")
                }
            }
        }
    }
    
    static func request(_ url : String, method : HTTPMethod, parameters : [String : Any]?, dataResponseJSON: @escaping handlerResponseJSON) {
        let data = Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: MyMovieConnection.sharedConnection.headers)
        data.responseJSON { (response) in
            MyMovieConnection.getCookies(response: response)
            if response.response?.statusCode == 403 {
                NotificationCenter.default.post(name: NSNotification.Name.init("expiredSessionObserver"), object: nil)
            }
            dataResponseJSON(response)
        }
    }
    
    //MARK: METODOS DA CONNECTION
    static var task = URLSessionDataTask()
    static var session = URLSession.shared
    
    static func makeHTTPGetRequest(_ url: String, onCompletion: @escaping ServiceResponse) {
        guard let urlConta = URL(string: url)  else {
            DispatchQueue.main.async(execute: { () -> Void in
                onCompletion(nil, nil)
            })
            return
        }
        
        Alamofire.request(urlConta).responseJSON() { dataResponse in
            switch dataResponse.result {
            case .success(let value):
                let myReponse = JSON(value)
                DispatchQueue.main.async(execute: { () -> Void in
                    onCompletion(myReponse, dataResponse.result.error as NSError?)
                })
                
            case .failure(let error):
                
                let myReponse = JSON(error.localizedDescription)
                DispatchQueue.main.async(execute: { () -> Void in
                    onCompletion(myReponse, dataResponse.result.error as NSError?)
                })
            }
        }
        
    }
    
    static func makeHTTPPostRequest(_ url: String, postParams : Parameters, onCompletion: @escaping ServiceResponse) {
        guard let urlConta = URL(string: url)  else {
            DispatchQueue.main.async(execute: { () -> Void in
                onCompletion(nil, nil)
            })
            return
        }
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 4
        
        manager.request(urlConta, method: .post, parameters: postParams,  encoding: JSONEncoding.default) .responseJSON { response in
            switch response.result {
            case .success(let value):
                let myReponse = JSON(value)
                DispatchQueue.main.async(execute: { () -> Void in
                    onCompletion(myReponse, response.result.error as NSError?)
                })
                
            case .failure(let error):
                
                if error._code == NSURLErrorTimedOut {
                    //timeout here
                    print("\n\nAuth request failed with error:\n \(error)")
                }
                
                print("\n\n===========Error===========")
                print("Error Code: \(error._code)")
                print("Error Messsage: \(error.localizedDescription)")
                if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                    print("Server Error: " + str)
                }
                debugPrint(error as Any)
                print("===========================\n\n")
                
                
                let myReponse = JSON(error.localizedDescription)
                DispatchQueue.main.async(execute: { () -> Void in
                    onCompletion(myReponse, response.result.error as NSError?)
                })
            }
        }
    }
    
    static func makeHTTPPutRequest(_ url: String, postParams : Parameters, onCompletion: @escaping ServiceResponse) {
        
        guard let urlConta = URL(string: url)  else {
            DispatchQueue.main.async(execute: { () -> Void in
                onCompletion(nil, nil)
            })
            return
        }
        
        Alamofire.request(urlConta, method: .put, parameters: postParams,  encoding: JSONEncoding.default) .responseJSON { response in
            switch response.result {
            case .success(let value):
                let myReponse = JSON(value)
                DispatchQueue.main.async(execute: { () -> Void in
                    onCompletion(myReponse, response.result.error as NSError?)
                })
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func makeHTTPDeleteRequest(_ url: String, postParams : Parameters, onCompletion: @escaping ServiceResponse) {
        
        guard let urlConta = URL(string: url)  else {
            DispatchQueue.main.async(execute: { () -> Void in
                onCompletion(nil, nil)
            })
            return
        }
        
        Alamofire.request(urlConta, method: .delete, parameters: postParams,  encoding: JSONEncoding.default) .responseJSON { response in
            switch response.result {
            case .success(let value):
                let myReponse = JSON(value)
                DispatchQueue.main.async(execute: { () -> Void in
                    onCompletion(myReponse, response.result.error as NSError?)
                })
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func removeSession() {
        for cookie in (Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.cookies)! {
            Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.deleteCookie(cookie)
        }
        MyMovieConnection.sharedConnection.headers = nil
        MyMovieConnection.sharedConnection.cookies = [HTTPCookie]()
        MyMovieConnection.sharedConnection.stringCookies = ""
    }
    
    
    static func requestWith(endUrl: String, imageData: [Data]?, parameters: Parameters, dataResponseJSON: @escaping handlerResponseJSON){
        let url = endUrl
        
        //        guard let urlConta = URL(string: url)  else {
        //            DispatchQueue.main.async(execute: { () -> Void in
        //                onCompletion(nil, nil)
        //            })
        //            return
        //        }
        
        let header: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var n = 0
            
            for data in imageData!{
                let propriedadeArquivo = "fotos_" + String(n)
                multipartFormData.append(data, withName: propriedadeArquivo, fileName: "image.png", mimeType: "image/png")
                n += 1
            }
            
            //            for (key, value) in objeto {
            //                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: "endereco." + key)
            //            }
            
            let jsonResult = JSON( parameters)
            print("Resultado: \(jsonResult)")
            multipartFormData.append("\(String(describing: jsonResult))".data(using: .utf8)!, withName: "dados",  mimeType: "application/json")
            
            
        },to: url, method: .post, headers: header,   encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    dataResponseJSON(response)
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    
    static func uploadImagem(endUrl: String, imageData: Data, objeto: [String : Any], dataResponseJSON: @escaping handlerResponseJSON){
        let url = endUrl
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in objeto {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            
            multipartFormData.append(imageData, withName: "perfilArquivo", fileName: "image.png", mimeType: "image/png")
            
        },to: url, method: .post,   encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    dataResponseJSON(response)
                }
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    
}

