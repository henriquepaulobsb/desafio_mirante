//
//  Connectivity.swift
//  mymovie
//
//  Created by Paulo Silva on 27/03/19.
//  Copyright © 2019 ph.mobi.br. All rights reserved.
//

import Foundation

import Alamofire

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

