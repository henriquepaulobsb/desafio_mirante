//
//  Service.swift
//  mymovie
//
//  Created by Paulo Silva on 27/03/19.
//  Copyright © 2019 ph.mobi.br. All rights reserved.
//

import UIKit
import SwiftyJSON

class Service: NSObject {

    public func getFilmesService(number: Int, handlerJsonResult:@escaping (JSON?) -> ()) {
        let url : String = Memoria.URL_SERVIDOR_PAGINADA + "\(number)"
        MyMovieConnection.makeHTTPGetRequest(url, onCompletion: { (jsonResult, error) -> Void in
            handlerJsonResult(jsonResult)
        })
    }
}
